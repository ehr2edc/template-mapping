Template-Mapping 
================


*  Contains empty template file (mapping-to-omop.csv)
*  Contains example with realistic examples 
*  Contains procedure / naming explanation...


Goal
====
Mapping local Concepts to OMOP concepts (standard concepts or any omop concepts)


Method
======
- for each medical domain create a csv file *<CHU_EHRNAME_DOMAIN.csv>*
- CHU : hospital/Medical Structure Name 
- EHRNAME : Local Electronic Health Record Identification  
- DOMAIN : Medical domain (diagnosis, patient, condition ...)
- eg: APHP_ORBIS_diagnosis.csv*
- use the corresponding template file (template-simple-format.csv | template-advanced-format.csv)


Mapping to omop concepts Format
================
- local_concept_name: the local terminology label (**MANDATORY**)
- local_concept_code: the local terminology concept code  (**MANDATORY**)
- local_domain: comment for the source domain (Ex 'Visit', 'Condition')
- local_vocabulary: comment for the source vocabulary (Ex 'APHP_Orbis_CIM10') should be equal to csv name (**MANDATORY**)
- local_language: comment for the source language (Ex 'EN', 'FR')
- local_ct: the percentiles (1 is most frequent, 4 is less)
- local_group: if items are linked together AND have to produce one unique standard concept, use this columns as a unique key. The model have to be CARESITE_VOCABULARY_LOCALCONCEPTCODE (Ex APHP_CIM10_733.6)
- standard_concept_id: if mapped, the OMOP standard equivalent concept id
- standard_concept_name: if mapped, the OMOP standard concept_name
- standard_concept_code: if mapped, the OMOP standard equivalent concept code (**MANDATORY**)
- standard_domain: comment for the standard domain (Ex 'Visit', 'Condition')
- standard_vocabulary: comment for the standard vocabulary (Ex 'SNOMED'...) (**MANDATORY**)
- mapping_relationship: Predicate for mapping (Ex 'Maps To', 'Narrower than') (**DO NOT USE**)
- mapping_score: estimated probability of the mapping value (between 0 and 1)
- mapping_comment: comment from the concept mapping responsible to track information


To edit this file, rst format is used 
here is the reference : http://docutils.sourceforge.net/docs/user/rst/quickref.html

